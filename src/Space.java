import com.sun.corba.se.impl.orbutil.graph.Graph;

import javax.swing.*;
import javax.swing.Timer;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.ImageProducer;
import java.io.IOException;
import java.util.*;
import java.util.List;
public class Space extends JPanel  implements ActionListener ,Runnable {
    public static enum State {
        Menu, Space, Magazine, Quit
    }

    ;
    static public int KillLifes = 50;
    public static int Moneys = 0;
    static MoveLayer layer = new MoveLayer();
    static public int BulPos = layer.X_plain;
    PlayAudio playAudio = new PlayAudio();
    boolean isread = false;
    WriteMonet w = new WriteMonet();
    Random random = new Random();
    static GenerateBossBullet bossBullet = new GenerateBossBullet();
    static Image Plain = new ImageIcon("C:\\Users\\Interstellar\\Desktop\\plainsGame\\superPlain.png").getImage();
    Image Space5 = new ImageIcon("C:\\Users\\Interstellar\\Desktop\\plainsGame\\space5.png").getImage();
    Image Space1 = new ImageIcon("C:\\Users\\Interstellar\\Desktop\\plainsGame\\space1.png").getImage();
    Timer timer = new Timer(1000 / 60, this);
    Image Heart = new ImageIcon("C:\\Users\\Interstellar\\Desktop\\plainsGame\\life.png").getImage();
    Thread enemy = new Thread((Runnable) this);
    GenerateCoin generateCoin = new GenerateCoin();
    GenerateBulletForColectr generateBulletForColectr = new GenerateBulletForColectr();
    Magzine magzine;
    int delay = 1000;
    public static double count = 100;
    public int eNEMYS = 0;
    DoArmagedon doArmagedon = new DoArmagedon();
    PosBos posBos = new PosBos();

    LinkedList<Enemy3> enemy3List = new LinkedList<Enemy3>();
    GenerateBullet generateBullet = new GenerateBullet();
    IncreaseLife increaseLife = new IncreaseLife();
    GetMoney1 getMoney1 = new GetMoney1();
    bullet bullet;
    private Menu menu;
    int Life = 1700000;

    public void StartGame() {
        timer.start();
        enemy.start();
        posBos.start();
        generateBulletForColectr.start();
        increaseLife.start();
        playAudio.start();
        bossBullet.start();
        generateCoin.start();
        doArmagedon.start();

    }

    Space() {

    }

    Space(String s) {

        StartGame();
        magzine = new Magzine();
        menu = new Menu();
        this.addMouseListener(new MouseInput());
        addKeyListener(new MoverAdapter());
        setFocusable(true);
    }

    public static State state = State.Menu;

    public void paint(Graphics g) {
        if (Space.state == State.Magazine) {
            magzine.RenderMag(g);

        }
        g = (Graphics2D) g;

        ((Graphics2D) g).drawImage(Space5, 0, layer.Layer, getWidth(), getHeight(), null);
        if (state == State.Space) {
            Moneys += 5;
            if (layer.Score <= 500) {
                ((Graphics2D) g).drawImage(Space5, 0, layer.Layer, getWidth(), getHeight(), null);
                ((Graphics2D) g).drawImage(Space5, 0, layer.Layer2, getWidth(), getHeight(), null);
            } else {
                ((Graphics2D) g).drawImage(Space1, 0, layer.Layer, getWidth(), getHeight(), null);
                ((Graphics2D) g).drawImage(Space1, 0, layer.Layer2, getWidth(), getHeight(), null);
            }

            if (layer.Score > 1200) {
                ((Graphics2D) g).drawImage(Space1, 0, 0, getWidth(), getHeight(), null);

                delay = 500;
            }
            if (layer.Score >= 1700) {


            }
            ((Graphics2D) g).drawImage(Plain, layer.X_plain, layer.Y_plain, null);

            Iterator<GameCoin> gameCoinIterator = generateCoin.gameCoinList.iterator();
            while (gameCoinIterator.hasNext()) {
                GameCoin gameCoin = gameCoinIterator.next();
                if (gameCoin.Coin_y > 600) {
                    gameCoinIterator.remove();
                } else {
                    gameCoin.MOveCOin();
                    ((Graphics2D) g).drawImage(gameCoin.Coin, gameCoin.Coin_x, gameCoin.Coin_y, null);
                }
            }
            g.setColor(Color.CYAN);
            Font font = new Font("Arial", Font.BOLD, 25);
            g.setFont(font);
            g.drawString(String.valueOf(count) + " %", 715, 60);
            g.drawString("Score-->" + (int) layer.Score, 630, 95);
            g.drawString("BossLife=" + Life, 39, 45);
            PaintHeart(g);
            if (Magzine.StateOfMag == Magzine.Plains.Plani1) {
                BulPos = layer.X_plain + 30;
            } else {
                BulPos = layer.X_plain;
            }
             GenBull(g);
             GenerateBossBullet(g);
            Iterator<Enemy3> i = enemy3List.iterator();
            while (i.hasNext()) {
                Enemy3 e = i.next();
                if (e.Pos_y >= 650 && doArmagedon.is_Done == true) {
                    i.remove();

                } else {
                    e.DefEnemy();
                    ((Graphics2D) g).drawImage(e.enemy3, e.Pos_x, e.Pos_y, 90, 67, null);

                }
            }


            Iterator<Armagedon> doArmagedonIterator = doArmagedon.armagedonList.iterator();
            while (doArmagedonIterator.hasNext()) {
                Armagedon armagedon = doArmagedonIterator.next();
                if (armagedon.Met_y > 600) {
                    doArmagedonIterator.remove();
                } else {
                    if (doArmagedon.isAlive()) {
                        doArmagedon.drawwarning(g);
                    }

                    armagedon.ArmagedonCreate();
                    ((Graphics2D) g).drawImage(armagedon.Armaged, armagedon.Met_x, armagedon.Met_y, null);

                }
                if (armagedon.GetMEteor().intersects(layer.GetPlain())) {
                    doArmagedonIterator.remove();
                    count -= 8;
                }

            }
            if (count <= 0) {
                ((Graphics2D) g).drawImage(Space5, 0, layer.Layer, getWidth(), getHeight(), null);
                state = State.Menu;

            }
            Iterator<boss> bossIterator = posBos.bosses.iterator();
            while (bossIterator.hasNext()) {
                boss boss1 = bossIterator.next();
                if (boss.x != posBos.x1) {
                    bossIterator.remove();
                } else
                    g.drawImage(boss1.bos, boss1.x, boss1.y, null);
                System.out.println(posBos.bosses.size());
            }
            Iterator<ColectableBullet> generateBullets = generateBulletForColectr.colectableBulletList.iterator();
            while (generateBullets.hasNext()) {
                ColectableBullet colectableBullet = generateBullets.next();
                if (colectableBullet.y >= 600) {
                    generateBullets.remove();
                } else {
                    colectableBullet.BulletGen();
                    ((Graphics2D) g).drawImage(Magzine.Bullets, colectableBullet.x, colectableBullet.y, null);
                }
            }
            Iterator<Heart> increaseLifeIterator = increaseLife.hearts.iterator();
            while (increaseLifeIterator.hasNext()) {
                Heart h = increaseLifeIterator.next();
                if (h.Heart_y > 600) {
                    increaseLifeIterator.remove();
                } else {
                    h.MoveHeart();
                    ((Graphics2D) g).drawImage(h.heart, h.Heart_x, h.Heart_y, null);
                }
            }
        }
        if (state == State.Menu) {
            menu.Render(g);

        }
        if (state == State.Magazine) {
            magzine.RenderMag(g);
        }


        if (Magzine.direction == Magzine.Direction.Left) {
            ((Graphics2D) g).drawImage(Space5, 0, layer.Layer, getWidth(), getHeight(), null);
            magzine.RenderNExtPage(g);
        }
        if (Magzine.StateOfMag == Magzine.Plains.Amunation) {
            ((Graphics2D) g).drawImage(Space5, 0, layer.Layer, getWidth(), getHeight(), null);
            magzine.RenderAmmo(g);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (state == State.Space) {
            layer.move();
            repaint();
            TestColWithArm();
            TestHeart();
            TestColisionBulletsForCollect();
            TesTColBosWithBull();
            TestCoin();


        }
        if (count <= 0) {
            state = State.Menu;

        }
        if (state == State.Menu) {
            repaint();

        }

        if (state == State.Magazine || state == State.Menu) {
            repaint();

        }


    }

    private void TestHeart() {
        Iterator<Heart> heartIterator1 = increaseLife.hearts.iterator();
        while (heartIterator1.hasNext()) {
            Heart h = heartIterator1.next();
            if (h.GetHeart().intersects(layer.GetPlain())) {
                heartIterator1.remove();
                count += 15;
            }
        }
    }


    private class MoverAdapter extends KeyAdapter {
        public void keyPressed(KeyEvent e) {

            layer.MovePlain(e);
            generateBullet.KeyPressed(e);
        }


        public void keyReleased(KeyEvent e) {
            layer.stopPlain(e);
        }
    }

    public void run() {
        while (layer.Score <= 1600) {
            try {
                if (state == State.Space)
                    enemy3List.add(new Enemy3(random.nextInt(650), 0, 4, this));
                Thread.sleep(delay);

            } catch (InterruptedException e) {
            }
        }

    }

    public void PaintHeart(Graphics graphics2D) {
        graphics2D.drawImage(Heart, 670, 25, null);
    }

    public void GenBull(Graphics g) {
        g.drawString("Bullets=" + GenerateBullet.bulletCount, 30, 80);
        Iterator<bullet> bulletIterator = generateBullet.bulletList.iterator();
        Iterator<Enemy3> e = enemy3List.iterator();
        while (bulletIterator.hasNext() && e.hasNext()) {
            bullet bulle1 = bulletIterator.next();
            Enemy3 e1 = e.next();
            if (bulle1.Bul_y <= 0) {
                bulletIterator.remove();
            } else {
                bulle1.shoot();

                if (layer.Score < 700) {
                    ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos + 20, bulle1.Bul_y, null);
                }
                if (layer.Score > 700) {

                    ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos + 20, bulle1.Bul_y, null);
                    ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos - 20, bulle1.Bul_y, null);
                    if (layer.Score >= 1400) {

                        ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos + 20, bulle1.Bul_y, null);
                        ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos - 20, bulle1.Bul_y, null);
                        ((Graphics2D) g).drawImage(bulle1.Bullet, BulPos + 60, bulle1.Bul_y, null);
                    }
                }

            }



        }
    }


    public void TestColWithArm() {
        Iterator<Armagedon> doArmagedonIterator = doArmagedon.armagedonList.iterator();
        Iterator<bullet> geb = generateBullet.bulletList.iterator();
        while (doArmagedonIterator.hasNext() && geb.hasNext()) {
            Armagedon armagedon = doArmagedonIterator.next();
            bullet b = geb.next();
            if (b.getRect().intersects(armagedon.GetMEteor())) {
                doArmagedonIterator.remove();
                geb.remove();
            }
        }
    }

    public void TestCoin() {
        Iterator<GameCoin> gameCoinIterator = generateCoin.gameCoinList.iterator();
        while (gameCoinIterator.hasNext()) {
            GameCoin gameCoin = gameCoinIterator.next();
            if (gameCoin.GetCoin().intersects(layer.GetPlain())) {
                gameCoinIterator.remove();
                w.money += 140;
            }
        }
    }

    private void TestColisionBulletsForCollect() {
        Iterator<ColectableBullet> generateBullets = generateBulletForColectr.colectableBulletList.iterator();
        while (generateBullets.hasNext()) {
            ColectableBullet colectableBullet = generateBullets.next();
            if (colectableBullet.GetBullet().intersects(layer.GetPlain())) {
                GenerateBullet.bulletCount += 30;
                generateBullets.remove();
            }
        }

    }

    private void TesTColBosWithBull() {
        Iterator<boss> bossIterator = posBos.bosses.iterator();
        Iterator<bullet> bulletIterator = generateBullet.bulletList.iterator();
        while (bossIterator.hasNext() && bulletIterator.hasNext()) {
            boss boss1 = bossIterator.next();
            bullet b = bulletIterator.next();
            if (boss1.GetBos().intersects(b.getRect())) {
                Life -= KillLifes;
            }
        }
    }

    private void TestColwitEnemy() {
        Iterator<bullet> bulletIterator = generateBullet.bulletList.iterator();
        Iterator<Enemy3> enemy3Iterator = enemy3List.iterator();
        while (bulletIterator.hasNext() && enemy3Iterator.hasNext()) {
            bullet b = bulletIterator.next();
            Enemy3 e = enemy3Iterator.next();
            if (b.getRect().intersects(e.getRect())) {
                bulletIterator.remove();
                enemy3Iterator.remove();
            }
        }

    }

    public void GenerateBossBullet(Graphics g) {
        Iterator<Bos_bullet> bos_bulletIterator = bossBullet.bos_bullets.iterator();
        while (bos_bulletIterator.hasNext()) {
            Bos_bullet bo = bos_bulletIterator.next();
            if (bo.y >= 600) {
                bos_bulletIterator.remove();
            } else {
                bo.ShotBossBull();
                g.drawImage(bo.Bul_boss, bo.x, bo.y, null);

            }
            if (bo.GetBUllet().intersects(layer.GetPlain())) {
                count -= 5;
                bos_bulletIterator.remove();

            }
        }
    }
}













