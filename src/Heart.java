import javax.swing.*;
import java.awt.*;

public class Heart {
    public Image heart=new ImageIcon("C:\\Users\\Interstellar\\Desktop\\plainsGame\\life.png").getImage();
    int Heart_x;
    int Heart_y;
    int Heart_Speed=10;
    public Heart(int x,int y){
        Heart_x=x;
        Heart_y=y;
    }
    public void MoveHeart(){
        Heart_y=Heart_y+Heart_Speed;
    }
    public Rectangle GetHeart(){
        return  new Rectangle(Heart_x,Heart_y,50,50);
    }
}
